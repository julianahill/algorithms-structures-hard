import java.util.HashMap;
import java.util.Stack;
import java.util.Map;

import java.lang.StringBuffer;
import java.lang.Character;
import java.lang.String;

public class WordFrequency {
 /**
  * wordFrequency parses a text file given as a String, and orders the Strings
  * according to its frequency in the text file.  This is accomplished in O(n),
  * assuming words are separated by whitespaces (newlines, tabs, spaces, etc.)
  * where n is equal to the number of characters in the given string.
  * Returns null if text is empty or results is less than or equal to zero.
  * It parses through all characters once with a loop in O(n), creating groups
  * that all other loops use, and, therefore are less than n.
  *
  * @param[in] text The text file represented as a String to be parsed.
  * @param[in] results The number of elements that should be returned.
  * @param[out] result The elements sorted according to frequency
  *
  * @return a sorted array of Strings
  */
  public static String[] wordFrequency(String text, int results) {
    if(text.length()==0 || results <= 0) return null;

    String[] result = new String[results];
    HashMap<String, Integer> count = new HashMap<String, Integer>();
    Stack<StringBuffer> stack = new Stack<StringBuffer>();
    StringBuffer temp = new StringBuffer("");

    // loops n times, adds each char that is not a whitespace to a temp
    // StringBuffer, then adds temp to a Stack to be put into a HashMap later
    // StringBuffer appends in constant time
    for(int i = 0; i < text.length(); ++i) {
      if(i!=text.length()-1 && !Character.isWhitespace(text.charAt(i))){
        temp.append(text.charAt(i));
      } else {
        if(i==text.length()-1 && !Character.isWhitespace(text.charAt(i))) {
          temp.append(text.charAt(i));
        } if(temp.length()>0) {
          stack.push(temp);
        } temp = new StringBuffer("");
      }
    }

    // loops size of stack * length of each StringBuffer
    // then adds each converted StringBuffer to the count HashMap that keeps
    // track of each String's frequency
    while(!stack.isEmpty()) {
      String string = stack.pop().toString();
      if(!count.containsKey(string)) {
        count.put(string, 1);
      } else {
        count.put(string, count.get(string)+1);
      }
    }

    // loops results times, get the biggest value of the count HashMap
    int biggest = 0;
    for(Map.Entry<String,Integer> e : count.entrySet()) {
      if(e.getValue() > biggest) biggest = e.getValue();
    }

    // loops results times, count the number of each number in the range of 0 to biggest
    int[] range = new int[biggest+1];
    for(Map.Entry<String, Integer> e : count.entrySet()) {
      range[e.getValue()]++;
    }

    // loops biggest times, gives correct position for each number of count to
    // be positioned into the results array
    for(int i=0, c=0; i < biggest+1; ++i) {
      if(range[i]==0) {
        range[i]=-1;
      } else {
        int t = c+range[i];
        range[i]+=c;
        c=t;
      }
    }

    // loops results times, fills in the result array to be returned in correct
    // order.
    for(Map.Entry<String, Integer> e: count.entrySet()) {
      if(range[e.getValue()]<0) continue;
      result[results-(range[e.getValue()]--)] = e.getKey();
    } return result;
  }
}
