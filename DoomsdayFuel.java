import java.lang.Math;

public class DoomsdayFuel {
  // Markov Chain
  public static int[] answer(int[][] m) {
    if(m == null) return null;
    if(m.length==1) {
        int[] res = {1,1};
        return res;
    }

    int[] total = new int[m.length];
    int[][] n = new int[m.length][m.length];

    // get each row's denominator
    for(int i=0; i < m.length; ++i) {
      for(int j=0; j < m.length; ++j) {
        total[i]+=m[i][j];
      }
    }

    // get denominators for the matrix
    for(int i=0; i < m.length; ++i) {
      for(int j=0; j < m.length; ++j) {
        if(m[i][j]!=0) {
          n[i][j] = total[i];
        }
      }
    }

    // find all terminals
    int terminals = 0;
    int firstTerminal=-1;
    for(int i=0; i < m.length; ++i) {
      if(total[i]==0) {
        terminals++;
        if(firstTerminal==-1) firstTerminal=i;
      }
    }

    int[] results = new int[terminals+1];
    // arrange states to be in proper form
    firstTerminal = arrange(m, n, total, firstTerminal);

    // calc I-Q matrix
    findIQ(m, n, total, firstTerminal);

    // calc inverse constant of I-Q
    findInverse(m, n, firstTerminal);

    // compute F*R matrix
    computeFR(m, n, total, firstTerminal);

    // fix returned denominator
    int numcheck = 0;
    for(int i=firstTerminal; i < m.length; ++i) {
      if(m[0][i] == 0) {
        n[0][i]=0;
      }
    } for(int i=firstTerminal; i < m.length; ++i) {
      if(n[0][i] > total[0]) {
        total[0]=n[0][i];
      }
    } for(int i=firstTerminal; i < m.length; ++i) {
      if(n[0][i]==0) {
        continue;
      } else if(n[0][i]!=total[0]){
        m[0][i] = m[0][i]*(total[0]/n[0][i]);
        n[0][i] = total[0];
      } numcheck+=m[0][i];
    } if(numcheck != total[0]) {
      total[0] = numcheck;
    }

    results[results.length-1] = total[0];
    for(int i=0; i < results.length-1; ++i) {
      if(m[0][i+firstTerminal]==total[0]) {
        m[0][i+firstTerminal] = 1;
        results[results.length-1] = 1;
      } results[i] = m[0][i+firstTerminal];
    } return results;
  }

  // @TODO as the rows are switched, switch corresponding columns
  private static int arrange(int[][] m, int[][] n, int[] total, int firstTerminal) {
    while(!isSorted(total, firstTerminal)) {
      firstTerminal=-1;
      for(int i=0; i < total.length;++i) {
        if(total[i]==0 && firstTerminal==-1) {
          firstTerminal=i;
          break;
        }
      }

      for(int i=firstTerminal; i < total.length; ++i) {
        if(total[i]==0) continue;
        for(int j=0; j < m.length; ++j) {
          int tm = m[i][j];
          int tn = n[i][j];

          m[i][j] = m[i-1][j];
          m[i-1][j] = tm;

          n[i][j] = n[i-1][j];
          n[i-1][j] = tn;
        }
        int tt = total[i];
        total[i] = total[i-1];
        total[i-1] = tt;
      }
    } return firstTerminal;
  }

  private static boolean isSorted(int[] total, int firstTerminal) {
    for(int i=firstTerminal; i < total.length; ++i) {
      if(total[i]==0) continue;
      return false;
    } return true;
  }

  private static void findIQ(int[][] m, int[][] n, int[] total, int firstTerminal) {
    for(int i=0; i < firstTerminal; ++i){
      for(int j=0; j < firstTerminal; ++j) {
        if(i==j){
          m[i][j] = 1*total[i] - m[i][j];
        } else {
          m[i][j] = -m[i][j];
        } n[i][j] = total[i];

        int[] s = simplify(m[i][j],n[i][j]);
        m[i][j]=s[0];
        n[i][j]=s[1];
      }
    }
  }

  private static void findInverse(int[][] m, int[][] n, int firstTerminal) {
    int[][] in = new int[firstTerminal][firstTerminal];
    int[][] im = new int[firstTerminal][firstTerminal];

    // create the equivalent id matrix
    for(int i=0; i < firstTerminal; ++i) {
      in[i][i] = 1;
      im[i][i] = 1;
    }

    // compute augmented matrix of I-Q
    for(int i=0; i < firstTerminal; ++i) {
      for(int j=i+1; j < firstTerminal; ++j) {
        if(m[j][i] == 0) {
          continue;
        }
        int cn = n[i][i];
        int cd = m[i][i];

        int[] s = simplify(cn, cd);
        cn = s[0];
        cd = s[1];

        for(int l=0; l < firstTerminal; ++l) {
          if(m[i][l]!=0){
            n[i][l] = n[i][l]*cd;
            m[i][l] = m[i][l]*cn;

            s = simplify(m[i][l], n[i][l]);
            m[i][l] = s[0];
            n[i][l] = s[1];
          } if(im[i][l]!=0) {
            in[i][l] = in[i][l]*cd;
            im[i][l] = im[i][l]*cn;

            s = simplify(im[i][l], in[i][l]);
            im[i][l] = s[0];
            in[i][l] = s[1];
          }
        }

        // subtract row i from row j
        cn = n[i][i]*m[j][i];
        cd = m[i][i]*n[j][i];

        s = simplify(cn, cd);
        cn = s[0];
        cd = s[1];

        for(int k=i; k < firstTerminal; ++k) {
          int num = cn * m[i][k];
          int den = cd * n[i][k];

          s = simplify(num, den);
          num = s[0];
          den = s[1];

          int l = lcm(n[j][k], den);
          int tn = num*(l/den);

          m[j][k] = m[j][k]*(l/n[j][k]);
          n[j][k] = l;

          m[j][k] = m[j][k]-tn;

          s = simplify(m[j][k], n[j][k]);
          m[j][k] = s[0];
          n[j][k] = s[1];
        } for(int k=0; k < firstTerminal; ++k) {
          int num = im[i][k] * cn;
          int den = in[i][k] * cd;
          if(den == 0) {
            continue;
          } if(im[j][k]==0) {
            in[j][k] = den;
            im[j][k] = num*-1;
            s = simplify(im[j][k], in[j][k]);
            im[j][k] = s[0];
            in[j][k] = s[1];
            continue;
          }
          int l = lcm(in[j][k], den);
          int tn = num*(l/den);

          im[j][k] = im[j][k]*(l/in[j][k]);
          in[j][k] = l;

          im[j][k] = im[j][k]-tn;

          s = simplify(im[j][k], in[j][k]);
          im[j][k] = s[0];
          in[j][k] = s[1];
        }
      }

      for(int j=0; j < i; ++j) {
        if(m[j][i] == 0) {
          continue;
        }
        int cn = n[i][i];
        int cd = m[i][i];

        int[] s = simplify(cn, cd);
        cn = s[0];
        cd = s[1];

        for(int l=0; l < firstTerminal; ++l) {
          if(m[i][l]!=0){
            n[i][l] = n[i][l]*cd;
            m[i][l] = m[i][l]*cn;

            s = simplify(m[i][l], n[i][l]);
            m[i][l] = s[0];
            n[i][l] = s[1];
          } if(im[i][l]!=0) {
            in[i][l] = in[i][l]*cd;
            im[i][l] = im[i][l]*cn;

            s = simplify(im[i][l], in[i][l]);
            im[i][l] = s[0];
            in[i][l] = s[1];
          }
        }

        // subtract row i from row j
        cn = n[i][i]*m[j][i];
        cd = m[i][i]*n[j][i];

        s = simplify(cn, cd);
        cn = s[0];
        cd = s[1];

        for(int k=i; k < firstTerminal; ++k) {
          int num = cn * m[i][k];
          int den = cd * n[i][k];

          s = simplify(num, den);
          num = s[0];
          den = s[1];

          int l = lcm(n[j][k], den);
          int tn = num*(l/den);

          m[j][k] = m[j][k]*(l/n[j][k]);
          n[j][k] = l;

          m[j][k] = m[j][k]-tn;

          s = simplify(m[j][k], n[j][k]);
          m[j][k] = s[0];
          n[j][k] = s[1];
        } for(int k=0; k < firstTerminal; ++k) {
          int num = im[i][k] * cn;
          int den = in[i][k] * cd;
          if(den == 0) {
            continue;
          } if(im[j][k]==0) {
            in[j][k] = den;
            im[j][k] = num*-1;
            s = simplify(im[j][k], in[j][k]);
            im[j][k] = s[0];
            in[j][k] = s[1];
            continue;
          }
          int l = lcm(in[j][k], den);
          int tn = num*(l/den);

          im[j][k] = im[j][k]*(l/in[j][k]);
          in[j][k] = l;

          im[j][k] = im[j][k]-tn;

          s = simplify(im[j][k], in[j][k]);
          im[j][k] = s[0];
          in[j][k] = s[1];
        }
      }
    }

    // set augmented matrix to real matrix
    for(int r=0; r < firstTerminal; ++r){
      for(int c=0; c < firstTerminal; ++c) {
        m[r][c]=im[r][c];
        n[r][c]=in[r][c];
      }
    }
  }

  private static void computeFR(int[][] m, int[][] n, int[] total, int firstTerminal) {
    int[][] nums = new int[firstTerminal][m.length-firstTerminal];
    int[][] dens = new int[firstTerminal][m.length-firstTerminal];
    for(int r=0; r < firstTerminal; ++r) {
      for(int c=0; c < m.length-firstTerminal; ++c) {
        int[] t = dot(m, n, r, c+firstTerminal, firstTerminal);
        nums[r][c] = t[0];
        dens[r][c] = t[1];
      }
    }

    for(int r = 0; r < firstTerminal; ++r) {
      for(int c = 0; c < m.length-firstTerminal; ++c) {
        m[r][c+firstTerminal] = nums[r][c];
        n[r][c+firstTerminal] = dens[r][c];
      }
    }
  }

  private static int[] dot(int[][] m, int[][] n, int row, int col, int firstTerminal) {
    int[] total = new int[2];
    for(int i = 0; i < m.length; ++i) {
      if(total[1]==0 || total[0]==0) {
        total[0] = m[row][i]*m[i][col];
        total[1] = n[row][i]*n[i][col];

        int[] s = simplify(total[0], total[1]);
        total[0]=s[0];
        total[1]=s[1];
      } else {
        int cn = m[row][i]*m[i][col];
        int cd = n[row][i]*n[i][col];

        if(cn==0 || cd==0) {
          continue;
        }

        int[] s = simplify(cn, cd);
        cn = s[0];
        cd = s[1];

        int l = lcm(cd, total[1]);
        int tn = cn*(l/cd);

        total[0] = total[0]*(l/total[1]);

        total[0]+=tn;
        total[1]=l;

        s = simplify(total[0], total[1]);
        total[0]=s[0];
        total[1]=s[1];
      }
    } return total;
  }

  // get greatest common facter of a and b
  private static int gcf(int a, int b) {
    if(a==0||b==0) return 0;
    while(b > 0) {
      int t = b;
      b = a % b;
      a = t;
    } return a;
  }

  // get least common multiple of a and b
  private static int lcm(int a, int b) {
    if(a==0||b==0) return 0;
    return a * (b / gcf(a, b));
  }

  private static int[] simplify(int numerator, int denominator) {
    if(numerator==0 && denominator==0) {
      int[] t = new int[2];
      t[1] = 1;
      return t;
    } if(denominator < 0) {
      numerator=numerator*-1;
      denominator=Math.abs(denominator);
    } if(Math.abs(numerator)==denominator){
      if(numerator < 0) numerator=-1;
      else numerator=1;
      denominator=1;
    } else if(gcf(Math.abs(numerator),denominator)!=1 &&
              gcf(Math.abs(numerator),denominator)!=0) {
      int g = gcf(Math.abs(numerator),denominator);
      denominator=denominator/g;
      numerator=numerator/g;
    } int[] t = new int[2];
    t[0]=numerator;
    t[1]=denominator;
    return t;
  }

  public static void printMatrix(int[][] m, int[][] n) {
    for(int r=0; r < m.length; ++r) {
      for(int c=0; c < m.length; ++c) {
        System.out.print(m[r][c] + "/" + n[r][c] + " ");
      } System.out.println();
    } System.out.println();
  }

  // test it
  public static void main(String [] args) {
    int[][] m1 = {{0,1,0,0,0,1},{4,0,0,3,2,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0}};
    int[] res = answer(m1);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m2 = {{0,2,1,0,0},{0,0,0,3,4},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}};
    res = answer(m2);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m4 = {{0,1,0,1,0},{1,0,1,0,0},{0,1,0,0,1},{0,0,0,0,0},{0,0,0,0,0}};
    res = answer(m4);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m5 = {{0,0,0,0,0},{0,1,0,1,0},{1,0,1,0,0},{0,1,0,0,1},{0,0,0,0,0}};
    res = answer(m5);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m6= {{0,1,0,1,0},{1,0,1,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,1,0,0,1}};
    res = answer(m6);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m3 = {{0,0,0,1},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
    res = answer(m3);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m10 = {{0,1,0,1},{1,0,1,0},{0,1,0,1},{0,0,0,0}};
    res = answer(m10);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m7= {{0,0},{0,1}};
    res = answer(m7);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m8= {{0}};
    res = answer(m8);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();

    int[][] m9 = {{0,0,0,0,0,0,0,0,0,0},
                  {0,1,0,0,0,1,0,0,0,0},
                  {4,0,3,2,0,0,0,0,0,0},
                  {0,0,0,0,0,0,0,0,0,0},
                  {0,0,0,0,0,0,0,0,0,0},
                  {0,0,0,0,0,0,0,0,0,0},
                  {0,0,0,0,0,0,0,0,0,0},
                  {0,0,0,0,0,0,0,0,0,0},
                  {0,1,0,0,0,0,0,0,0,1},
                  {0,0,0,0,0,0,0,0,0,0}};
    res = answer(m9);
    for(int i=0; i < res.length; ++i) System.out.print(res[i]+" ");
    System.out.println();
  }
}
