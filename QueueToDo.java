import java.lang.Math;
import java.lang.String;

public class QueueToDo {
  /*public static int answer(int start, int length) {
      int xor=start;
      for(int i=1, r=length-1, l=1; i < length && r>=0; ++i) {
          if(i<=r) {
              xor = xor^(start+l);
          } if(i==r) {
              i=-1;
              l+=(length-r);
              r--;
          }  else {
            l++;
          }
      } return xor;
  }*/

  public static int answer(int start, int length) {
    int res=start;
    for(int i=0, l=0; i < length; ++i) {
        int a = start+i+l;
        System.out.print(a+"\t");
        l+=length-1;
        int b = start+l;
        System.out.print(b+"\t");
        if(i==0) {
          if(start==0){
            res = xor(1,b);
          } else {
            res = xor(a,b);
          }
        } else if(i!=length-1){
          res = res ^ xor(a,b);
        } else {
          res = res ^ a;
        } System.out.println(res);
    } return res;
  }

  private static int f(int a) {
    int res[] = {a,1,a+1,0};
    return res[a%4];
  }

  private static int xor(int a, int b) {
    return f(b)^f(a-1);
  }

  public static void main(String [] args) {
    /*System.out.println("start=0 length=3 expected=2: "+answer(0,3));
    System.out.println("start=1 length=3 expected=6: "+answer(1,3));
    System.out.println("start=2 length=3 expected=14: "+answer(2,3));
    System.out.println("start=3 length=3 expected=10: "+answer(3,3));
    System.out.println("start=4 length=3 expected=2: "+answer(4,3));
    System.out.println("start=5 length=3 expected=14: "+answer(5,3));
    System.out.println("start=6 length=3 expected=6: "+answer(6,3));
    System.out.println("start=7 length=3 expected=10: "+answer(7,3));
    System.out.println("start=8 length=3 expected=2: "+answer(8,3));
    System.out.println("start=9 length=3 expected=6: "+answer(9,3));
    System.out.println("start=10 length=3 expected=30: "+answer(10,3));
    System.out.println("start=11 length=3 expected=26: "+answer(11,3));
    System.out.println("start=12 length=4 expected=2: "+answer(12,3));
    System.out.println("start=17 length=3 expected=6: "+answer(17,3));
    System.out.println("start=20 length=3 expected=2: "+answer(20,3));*/
    /*System.out.println("start=0 length=4 expected=10: "+answer(0,4));
    System.out.println("start=1 length=4 expected=14: "+answer(1,4));
    System.out.println("start=10 length=4 expected=6: "+answer(10,4));
    System.out.println("start=17 length=4 expected=14: "+answer(17,4));*/
    //System.out.println("start=0 length=50000 expected=?:"+answer(0,50000));
  }
}
