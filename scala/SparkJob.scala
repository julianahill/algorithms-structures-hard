import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD

object SparkJob {
  def main(args: Array[String]) {
    // create Spark context with Spark configuration
    val conf = new SparkConf().setAppName("Spark Job")
    val sc = new SparkContext(conf)

    // assets json file
    if(args.isEmpty) println("missing file paths: <assets> <ad events>")
    val assets_path = args(0)
    val as_txt = sc.textFile(assets_path)

    // split lines of asset file by comma to collect json object fields
    val as_keys = as_txt.flatMap(line => line.split(","))

    // filter out lines that do not contain a pv
    val as_pvs = as_keys.filter(word => word.contains("\"pv\":"))
    val as_pv = as_pvs.flatMap(line => line.split(":").filter(k => !k.contains("\"pv\"")))

    // count each pv asset impression
    val as_pv_count = as_pv.map(x => (x, 1)).countByKey()

    // convert map to RDD
    val as_rdd: RDD[(String, Long)] = sc.parallelize(as_pv_count.toSeq).map{case (pv, c) => {(pv, c)}}

    // ad_events json file
    val ad_path = args(1)
    val ad_txt = sc.textFile(ad_path)

    // filter out ad_words that do not contain pv and view/click
    val ad_lines = ad_txt.filter(line => line.contains("\"pv\":") && line.contains("\"e\":") && (line.contains("\"view\"") || line.contains("\"click\"")))
    val ad_keys = ad_lines.map(line => (line.split(",").filter(l => l.contains("\"pv\":")).mkString(" ").split(":").filter(l => !l.contains("\"pv\"")).mkString(" "), line.split(",").filter(l => l.contains("\"e\":")).mkString(" ").split(":").filter(l => !l.contains("\"e\"")).mkString(" ")))

    // get rid of ad event pvs that are not in assets
    val ad_rdd = ad_keys.subtractByKey(ad_keys.subtractByKey(as_rdd))

    // separate and process clicks
    val ad_clicks = ad_rdd.filter(v => v._2.contains("click"))
    val ad_clicks_rdd: RDD[(String, Long)] = sc.parallelize(ad_clicks.countByKey().toSeq).map{case(pv,c) => {(pv,c)}}

    // separate and process views
    val ad_views = ad_rdd.filter(v => v._2.contains("view"))
    val ad_views_rdd: RDD[(String, Long)] = sc.parallelize(ad_views.countByKey().toSeq).map{case (pv,c) => {(pv,c)}}

    // cogroup pv views and clicks and stand alone assets
    val pv_result = as_rdd.cogroup(ad_views_rdd).cogroup(ad_clicks_rdd)

    // convert iterators to strings
    val pv_string = pv_result.map{ x => {
      val pv = x._1.split("\"").filter(x => !x.contains("\"")).mkString
      val value = x._2
      val it1 = value._1.map{y => {
        (y._1.mkString(" "))
      }}
      val it2 = value._1.map{y => {
        if(y._2.mkString.isEmpty)("0")
        else (y._2.mkString(" "))
      }}
      val a = it1.mkString(" ")
      val v = it2.mkString(" ")
      if(value._2.mkString.isEmpty) (pv,((a,v),"0"))
      else (pv, ((a, v),value._2.mkString(" ")))
    }}

    // print pv, asset impressions, views and clicks for each ad
    //for((pv,((a,v),c)) <- pv_string) println(pv + " " + a + " " + v + " " + c)
    pv_string.coalesce(1, true).saveAsTextFile("results")
  }
}
