# Spark Job

You need to write a Apache Spark application (job) to analyze combined data from
the two different files given below.

http://gumgum-share.s3.amazonaws.com/assets_2014-01-20_00_domU-12-31-39-01-A1-34.gz

http://gumgum-share.s3.amazonaws.com/ad-events_2014-01-20_00_domU-12-31-39-01-A1-34.gz

The file starting with assets_ contains asset (image) impressions. Each line
represents 1 asset impression.

The file starting ad-events_ contains various kinds of ad events. Each line
represents one ad event. We are interested only in two kinds of ad events -
`view` and `click`. Rest of the ad events should be ignored. Type of ad event is
specified in the value of the field `e`. You can see `"e":"view"` and `"e":"click"`
in the json messages.

Each line also has another id called `page view id`. This id is specified by
json key `pv`. You can find json key values similar to
`"pv":"7963ee21-0d09-4924-b315-ced4adad425f"` in both the files.

The aim is to join the data in two files using `pv`. You need to parse these
files and combine the data in two files to check how many asset impressions,
views and clicks are present in both the files for each page view id.

The output file should have four columns: page view id, asset impressions, views
and clicks:
```
7963ee21-0d09-4924-b315-ced4adad425f 3 2 0
6933ee21-0d09-9898-b315-ced4adad890o 5 0 0
9963ee21-0d09-6955-b315-ced4adad5697 3 2 1
```

You need to ignore all the rows with asset impressions = 0. It's possible that
some page view ids will have views and clicks but may not have any asset
impressions.

You should be able to accomplish this in one spark job. Please deliver your
spark job in a jar with clear instructions on how to run the job on locally
installed Spark. Feel free to use any version of Spark. You must use Scala​ for
writing the job.

Please submit the following artifacts after you are done:
1. Jar with classes involved in your job
2. Source code
3. Instructions about how to run your spark job
4. Output file generated as a result of running this job on your machine

Do not share your code using GitHub or BitBucket or any other publically
available git hosting repositories.
Your cost must be submitted as files to us. If your file size is too big,
please use online services such as https://www.wetransfer.com/ to send the
files to us. Please do not discuss or share your solution with anybody.
We may ask you further questions on your implementation.