import java.lang.String;

public class BigNumbers {
  public static String answer(int[] xs) {
    if(xs.length==1) return String.valueOf(xs[0]);

    boolean odd = false;
    int negatives = 0;
    String total = String.valueOf(0);

    for(int i=0; i < xs.length; ++i) {
        if(xs[i] > 0) {
            if(total.compareTo(String.valueOf(0))==0) {
                total = String.valueOf(xs[i]);
            } else {
                total = multiply(total, String.valueOf(xs[i]));
            }
        } else if(xs[i]!=0) {
            negatives++;
        }
    }

    if(negatives == 0 && total.compareTo(String.valueOf(0))==0) {
        return String.valueOf(0);
    } else if(negatives%2 != 0) {
        odd = true;
    }

    int big = 0;
    for(int i=0; i<xs.length && odd; ++i) {
        if(xs[i] >= 0) {
            continue;
        } else if(Math.abs(xs[i]) > Math.abs(big)) {
            big = xs[i];
        }
    }

    int small = big;
    for(int i=0; i<xs.length && odd; ++i) {
        if(xs[i] >= 0) {
            continue;
        } else if(Math.abs(xs[i]) < Math.abs(small)) {
            small = xs[i];
        }
    }

    for(int i=0; i<xs.length && odd; ++i) {
        if(xs[i]==small) {
            xs[i] = 0;
            odd = false;
        }
    }

    for(int i=0; i<xs.length && !odd; ++i) {
        if(xs[i] >= 0) {
            continue;
        } if(total.compareTo(String.valueOf(0))==0) {
            total = String.valueOf(Math.abs(xs[i]));
        } else {
            total = multiply(total, String.valueOf(Math.abs(xs[i])));
        }
    } return total;
}

private static String multiply(String one, String two) {
    // make the longest string the first string
    String o=one, t=two;
    if(one.length() < two.length()) {
        t=one;
        o=two;
    }

    // nested loop to multiple individual digits together
    String result = String.valueOf(0);
    int carry = 0;
    for(int i=t.length()-1, x=0; i >= 0 && x < t.length(); --i, ++x) {
        int i2 = Character.getNumericValue(t.charAt(i));

        // multiple digits together
        String temp = new String();
        for(int z=0; z < x; ++z) {
            temp = temp + String.valueOf(0);
        } for(int j=o.length()-1; j >= 0; --j) {
            int i1 = Character.getNumericValue(o.charAt(j));
            int total = carry + (i2*i1);

            temp = temp + String.valueOf(total%10);
            carry = total/10;
        }

        // added temp string to result
        result = add(result, temp);
    } if(carry!=0) {
      result=result+String.valueOf(carry);
    }

    // reverse the order of the digits
    String temp = result;
    result = new String();
    for(int i=temp.length()-1; i >=0; --i) {
        result = result + temp.charAt(i);
    } return result;
}

private static String add(String one, String two) {
    if(Integer.valueOf(one)==0) return two;
    if(Integer.valueOf(two)==0) return one;

    String o=one, t=two;
    if(one.length() < two.length()) {
        t=one;
        o=two;
    }

    int carry = 0;
    String result = new String();
    for(int i=0; i < o.length(); ++i) {
        int i1=0, i2=0;
        if(i <= t.length()-1) {
            i2 = Character.getNumericValue(t.charAt(i));
        } i1 = Character.getNumericValue(o.charAt(i));

        int total = carry + i1 + i2;
        if(total>=10) {
            result = result + String.valueOf(total-10);
        } else {
            result = result + String.valueOf(total);
        } carry = total/10;
    } if(carry!=0) {
      result=result + String.valueOf(carry);
    } return result;
  }


  public static void main(String [] args) {
    int[] a1 = new int[]{2,0,2,2,0};
    System.out.println("expected=8\toutput="+answer(a1));
    int[] a2 = new int[]{-2,-3,4,-5};
    System.out.println("expected=60\toutput="+answer(a2));
    int[] a3 = new int[]{2,-3,1,0,-5};
    System.out.println("expected=30\toutput="+answer(a3));
    int[] a4 = new int[]{0,0,0,0,0};
    System.out.println("expected=0\toutput="+answer(a4));
    int[] a5 = new int[]{-3};
    System.out.println("expected=-3\toutput="+answer(a5));
    int[] a6 = new int[]{-3,-7};
    System.out.println("expected=21\toutput="+answer(a6));
    int[] a7 = new int[50];
    for(int i=0; i < 50; ++i) a7[i] = 1000;
    System.out.println("output="+answer(a7));
    int[] a8 = new int[50];
    for(int i=0; i < 50; ++i) a8[i] = -1000;
    System.out.println("output="+answer(a8));
    int[] a9 = new int[]{3};
    System.out.println("expected=3\toutput="+answer(a9));
    int[] a10 = new int[]{-1,-1,-1};
    System.out.println("expected=1\toutput="+answer(a10));
    int[] a11 = new int[]{0,0,0,0,0,-1};
    System.out.println("expected=0\toutput="+answer(a11));
  }
}
