# Interview Challenges

## Evernote (WordFrequency)
Write a function that takes two parameters:
1. a String representing the contents of a text document
2. an integer providing the number of items to return

Implement the function such that it returns a list of Strings ordered by word
frequency, the most frequently occurring word first. Use your best judgement to
decide how words are separated. Implement this function as you would for a
production / commercial system. You may use any standard data structures.
Please use your choice of language.

Performance Constraints:
* Your solution should run in O(n) time where n is the number of characters in the
document.
* Please provide reasoning on how the solution obeys the O(n) constraint.

## Google - Foo.bar
1. _BigNumbers_: multiply an array of numbers together excluding zeros.
2. _QueueToDo_: xor a pascal triangle that contains sequential numbers as though
it were part of a matrix
3. _DoomsdayFuel_: find the Markov chain of a given matrix filled with integers
